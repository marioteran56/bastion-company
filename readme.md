# Practica de conexión de servidores con Bastion y Ansible

Esta práctica consiste en crear 4 diferentes servidores (bastion, HR, Sales y Accounting) dentro de contenedores con una imagen Debian. 

El servidor Bastion contiene Ansible instalado y se encarga de controlar los demás servidores por medio de un playbook que se encuentra adjunto en el repositorio como [playbook.yml](playbook.yml).

## ¿Cómo funciona?

Esta práctica utiliza un archvio **[docker-compose](docker-compose.yml)** en donde se crean una serie de contenedores a partir de dos distintos Dockerfiles.

**[DockerfileBas](DockerfileBas)** se utiliza para la creación de una imagen Debian con los paquetes de SSH, Ansible y Python, al igual que generamos las llaves publica y privada SSH. En este caso, creamos un contenedor llamado **bas**, que simboliza nuestro Bastion en el cual mediante volúmenes insertamos dentro del mismo los archivos [playbook.yml](playbook.yml) y [hosts](hosts)

```dockerfile
  bastion:
    build: 
      context: .
      dockerfile: DockerfileBas
    container_name: bas
    hostname: bastion
    tty: true
    volumes:
      - ./playbook.yml:/root/playbook.yml
      - ./hosts:/etc/ansible/hosts
```

**[DockerfileDef](DockerfileDef)** se utiliza para la creación de una imagen Debian con los paquetes de SSH y Python. En este caso, creamos 3 contenedores llamados **hr**, **sales** y **accounting** que simboliza nuestros otros servidores a los cuales accederemos mediante el Bastion Host mediante SSH.

```dockerfile
  hr:
    build: 
      context: .
      dockerfile: DockerfileDef
    container_name: hr
    hostname: hr
    tty: true
  sales:
    build: 
      context: .
      dockerfile: DockerfileDef
    container_name: sales
    hostname: sales
    tty: true
  accounting:
    build: 
      context: .
      dockerfile: DockerfileDef
    container_name: accounting
    hostname: accounting
    tty: true
```

## Uso

Para poder ejecutar correctamente los contenedores, primeramente ejecutamos dentro de nuestro proyecto el siguiente comando (tomando en cuenta que tenemos instalado **docker compose**): 

```bash
docker compose up -d
```

Seguido de esto, para que nuestro servidor Bastion pueda conectarse correctamente a los demás servidores(contenedores) debemos copiar la llave publica de nuestro Bastion a los demás servidores. Para esto, ejecutamos el siguiente comando:

```bash
docker cp bas:/root/.ssh/id_rsa.pub /tmp/id_rsa.pub
```

Donde estamos guardando nuestra llave pública en un archivo temporal dentro de nuestro ordenador. Luego tenemos que transferir dicho archivo a nuestros otros servidores, para esto ejecutamos los siguientes comandos:

```bash
docker exec -i hr sh -c 'cat > /root/.ssh/authorized_keys' < /tmp/id_rsa.pub
docker exec -i sales sh -c 'cat > /root/.ssh/authorized_keys' < /tmp/id_rsa.pub
docker exec -i accounting sh -c 'cat > /root/.ssh/authorized_keys' < /tmp/id_rsa.pub
```

Una vez hecho esto, agregamos la huella digital de las llaves SSH de nuestros servidores remotos en nuestro servidor Bastion, para esto ejecutamos el siguiente comando:

```bash
docker exec -it bas sh -c 'ssh-keyscan -f /tmp/remote-hosts.txt >> /root/.ssh/known_hosts'
```

Finalmente, para poder ejecutar nuestro playbook y que Ansible se conecte a los demás servidores, ejecutamos el siguiente comando:

```bash
docker exec -it bas sh -c 'ansible-playbook /root/playbook.yml'
```